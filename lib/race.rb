module RaceBet
  class Race

    class << self
      def score(guesses, winners)
        guesses = guesses[0..4]
        winners = winners[0..4]

        score = 0

        guesses.each_with_index do |guess, index|
          if guess == winners[index]
            case index
            when 0
              score += 15
            when 1
              score += 10
            when 2
              score += 5
            when 3
              score += 3
            when 4
              score += 1
            end
          elsif winners.include? guess
            score += 1
          end
        end

        score
      end
    end

  end
end
